﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using HTTPLib;
using System.Reflection;

namespace HTTPServer {
    public partial class Form1 : Form {
        // Fields
        Socket socListen;
        // List of plugins
        List<HTTPMethodHandler> lstPlugIns;
        // Dictionary for server configuration information
        private Dictionary<string, string> dictServer;

        public Form1() {
            InitializeComponent();
            // Initialize dictionary and add two entries:
            // ServerName and DocumentRoot
            dictServer = new Dictionary<string, string>();
            dictServer.Add("ServerName", "KingsPin");
            dictServer.Add("DocumentRoot", @"C:\Users\Owner\Documents\Test Files");
            vLoadPlugins();
        }




        private void vStopListening() {
            // Close the listening socket
            socListen.Close();
        }

        private void vStartListening() {
            // Create listening socket, bind to IP address/port num, tell to start
            socListen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // Local endpoint to listen on
            IPEndPoint ipeListen = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8080);
            socListen.Bind(ipeListen);
            socListen.Listen(5);
            // Start task to accept connection requests.
            Task.Run(() => vAcceptConnects());
        }

        private void vAcceptConnects() {
            // Loop forever, accepting requests
            while (true) {
                try {
                    Socket socConnection = socListen.Accept();
                    // Start a thread to handle this connection.
                    Task.Run(() => vConnect(socConnection));
                } catch (SocketException) {
                    // We have stopped listening for connections. return
                    return;
                }
            }
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            if (btnStartStop.Text == "Start") {
                vStartListening();
                btnStartStop.Text = "Stop";
            } else {
                vStopListening();
                btnStartStop.Text = "Start";

            }
        }

        // Recieve and parse HTTP request from the remote client
        private void vConnect(Socket socConnect) {
            NetworkStream nsConnection = new NetworkStream(socConnect);
            HTTPRequest hrtRequest = HTTPRequest.Receive(nsConnection);
            vChangeText(socConnect);
            string strMethod = hrtRequest.Method;
            foreach (HTTPMethodHandler hmhMethod in lstPlugIns) {
                if (strMethod.ToUpper() == hmhMethod.MethodName) {
                    HTTPResponse hrResponse = hmhMethod.GetResponse(hrtRequest, dictServer);
                    hrResponse.Send(nsConnection);
                    nsConnection.Close();
                    socConnect.Close();
                    return;
                }
            }
            string strStatus = "HTTP/1.1 405 Method Not Allowed";
            List<string> lstAllow = new List<string>();
            lstAllow.Add("Allow: GET OPTIONS");
            HTTPResponse hrR = new HTTPResponse(strStatus, lstAllow, null);
            hrR.Send(nsConnection);
            nsConnection.Close();
            socConnect.Close();
            return;
        }

        // Method that loads all plugins from Plugins folder
        private void vLoadPlugins() {
            // Create list of plugins
            lstPlugIns = new List<HTTPMethodHandler>();
            // Get list of files in Plugin Directory
            string[] strFiles = Directory.GetFiles("Plugins");
            // Loop through files, looking for DLLs
            foreach (string strOneFile in strFiles) {
                // Only DLLs
                if (Path.GetExtension(strOneFile).ToLower() == ".dll") {
                    Assembly aDLL = Assembly.LoadFrom(strOneFile);
                    Type[] tyTypesInDLL = aDLL.GetTypes();
                    // Loop through types
                    foreach (Type tyOneType in tyTypesInDLL) {
                        // Find whether type is derived from plugin
                        if (typeof(HTTPMethodHandler).IsAssignableFrom(tyOneType)) {
                            // Create instance of class and place in list
                            HTTPMethodHandler hmhOne = (HTTPMethodHandler)Activator.CreateInstance(tyOneType);
                            lstPlugIns.Add(hmhOne);
                        }
                    }
                }
            }
        }

        // Delegate to handle text
        private delegate void DelOneStringParam(string strTxt);
        // Change the text in the RTB
        private void vChangeText(Socket socConnection) {
            string strText = "";
            // Date and Time
            strText += DateTime.Now.ToString("r") + "\r\n";
            // Extract Remote End point
            IPEndPoint ipeRemote = socConnection.RemoteEndPoint as IPEndPoint;
            // IP
            strText += "Remote Address: " + ipeRemote.Address.ToString() + "\r\n";
            // Port Number
            strText += "Remote Port Number: " + ipeRemote.Port.ToString() + "\r\n\r\n";
            DelOneStringParam delTxt = new DelOneStringParam(vAddTxt);
            this.Invoke(delTxt, strText);
        }

        private void vAddTxt(string strTxt) {
            rtbLog.Text = strTxt;        }
    }
}