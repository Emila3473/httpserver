﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTTPLib {
    public abstract class HTTPMethodHandler {
        // Property telling the method name (e.g., GET); must be all uppercase letters.
        public abstract string MethodName {
            get;
        }

        // Method generating the response to an HTTP request using this method.
        // Assumes the method name in the request line matches this method name.
        public abstract HTTPResponse GetResponse(HTTPRequest hrRequest, Dictionary<string, string> dictServerConfig);

    }
}
