﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HTTPLib
{
    public class HTTPRequest
    {
        // Fields
        private string strMethod;
        private string strURI;
        private string strVersion;
        private List<string> lstHeaders;

        // Properties
        public string Method {
            get {
                return strMethod;
            }
        }

        public string URI {
            get {
                return strURI;
            }
        }

        public string Version {
            get {
                return strVersion;
            }
        }

        public List<string> Header {
            get {
                return lstHeaders;
            }
        }

        // Methods
        // Constructor. Initialize fields from the parameters passed in
        public HTTPRequest(string strM, string strU, string strV, List<string> lstH) {
            strMethod = strM;
            strURI = strU;
            strVersion = strV;
            lstHeaders = lstH;
        }

        public static HTTPRequest Receive(Stream strmInput) {
            // A string for line read and list of strings for headers
            string strOneLine;
            List<string> lstHdrs = new List<string>();
            // Read first line, which is request line
            strOneLine = ReadOneLine(strmInput);
            // Split request line on spaces
            char[] chSplit = new char[1];
            chSplit[0] = ' ';
            string[] strPieces = strOneLine.Split(chSplit);
            // Read header lines until read blank line
            while ((strOneLine = ReadOneLine(strmInput)) != "") {
                lstHdrs.Add(strOneLine);
            }
            // Finished. Convert to HTTPREquest object and return
            return new HTTPRequest(strPieces[0], strPieces[1], strPieces[2], lstHdrs);
        }

        // Enumeration 
        private enum OneLineStates {
            Line,
            CR,
            Done
        }
        // Read one line from the input stream
        private static string ReadOneLine(Stream strmInput) {
            // Initally reading line, empty list of bytes read so far
            OneLineStates olsCurrentState = OneLineStates.Line;
            List<byte> lstBytes = new List<byte>();
            // Read bytes as long as more bytes to read and not done yet
            int iNextByte = 0;
            while (olsCurrentState != OneLineStates.Done && (iNextByte = strmInput.ReadByte()) != -1) {
                // Look at current state and byte read to determine what to do
                switch (olsCurrentState) {
                    case OneLineStates.Line:
                        // Check whether read in CR (0x0D)
                        if (iNextByte == 0x0D) {
                            olsCurrentState = OneLineStates.CR;
                        } else {
                            // Byte part of current line
                            lstBytes.Add((byte)iNextByte);
                        }
                        break;

                    case OneLineStates.CR:
                        // Check whether read in LF (0x0A)
                        if (iNextByte == 0x0A) {
                            // End of line
                            olsCurrentState = OneLineStates.Done;
                        } else {
                            // Add CR and Byte to the list
                            lstBytes.Add(0x0D);
                            lstBytes.Add((byte)iNextByte);
                            olsCurrentState = OneLineStates.Line;
                        }
                        break;

                    case OneLineStates.Done:
                        // Never should get here. Throw exception
                        throw new Exception("Bug in 'ReadOneLine' method.");
                }
            }
            // Have finished reading one line. Convert to string and return
            byte[] byLineBytes = lstBytes.ToArray();
            string strLine = Encoding.ASCII.GetString(byLineBytes);
            return strLine;
        }

        // Override ToString method
        public override string ToString() {
            // Build up the output string in pieces
            string strOutput = "";
            // Show the method
            strOutput += "Method: " + this.strMethod + "\r\n";
            // Show the URI
            strOutput += "URI: " + this.strURI + "\r\n";
            // Show the version
            strOutput += "Version: " + this.strVersion + "\r\n";
            // List Headers
            strOutput += "headers:\r\n";
            foreach (string strOneHdr in lstHeaders) {
                strOutput += "\t" + strOneHdr + "\r\n";
            }
            return strOutput;
        }
    }
}
