﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HTTPLib {
    public class HTTPResponse {
        // Fields
        string strStatusLine;
        List<string> lstResponseHeaders;
        Stream strmResponseBody;

        // Methods
        // Constructor
        public HTTPResponse(string strStLn, List<string> lstRspHdrs, Stream strmRspBdy) {
            strStatusLine = strStLn;
            lstResponseHeaders = lstRspHdrs;
            strmResponseBody = strmRspBdy;
        }

        public void Send(Stream strmOut) {
            byte[] byStLn = Encoding.ASCII.GetBytes(strStatusLine + "\r\n");
            strmOut.Write(byStLn, 0, byStLn.Length);
            foreach (string strHeader in lstResponseHeaders) {
                byte[] byHdr = Encoding.ASCII.GetBytes(strHeader + "\r\n");
                strmOut.Write(byHdr, 0, byHdr.Length);
            }
            byte[] byEnd = Encoding.ASCII.GetBytes("\r\n\r\n");
            strmOut.Write(byEnd, 0, byEnd.Length);
            byte[] byRspBdy = new byte[1024];
            int iNumBytesRead = 0;
            if (strmResponseBody != null) {
                iNumBytesRead = strmResponseBody.Read(byRspBdy, 0, 1024);
                while (iNumBytesRead > 0) {
                    strmOut.Write(byRspBdy, 0, iNumBytesRead);
                    iNumBytesRead = strmResponseBody.Read(byRspBdy, 0, 1024);
                }
                strmResponseBody.Close();
            }
        }
    }
}
